//
//  Comments.swift
//  Travel App
//
//  Created by Михаил on 31/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class Comment {
    var postId: Int = 0
    var id: Int = 0
    var name: String = ""
    var email: String = ""
    var body: String = ""
}
