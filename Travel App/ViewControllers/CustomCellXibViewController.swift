//
//  CustomCellXibViewController.swift
//  Travel App
//
//  Created by Михаил on 14/06/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class CustomCellXibViewController: UIViewController {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let xib = UINib(nibName: "CustomXibCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "cell")
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "user avatar did changed"), object: nil, queue: nil) { (notofication) in
            print("Случилось событие!!!!!!! \(notofication.object)")
            self.topLabel.text = notofication.object as! String
        }
    }
    

}


extension CustomCellXibViewController: UITableViewDataSource, UITabBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
    
    
}
