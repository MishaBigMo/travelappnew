//
//  UsersListViewController.swift
//  Travel App
//
//  Created by Михаил on 17/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class UsersListViewController: UIViewController {
    var users: [RealmUsers] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ApiManager.instance.getUsersDecodable { (userFromServer) in
            for user in userFromServer! {
                let realmUser = RealmUsers()
                realmUser.id = String(user.id)
                realmUser.name = user.name ?? "no data"
                realmUser.email = user.email ?? "no data"
                self.users.append(realmUser)
            }
            DataBaseManager.instance.saveUserToDataBase(users: self.users)
            //self.tableView.reloadData()
        }
        users = DataBaseManager.instance.getUsersFromDataBase()
    }
}


extension UsersListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let user = users[indexPath.row] 
        cell.userNameLabel.text = user.name
        cell.label2.text = user.id
        cell.label3.text = user.email
        return cell
    }
}

