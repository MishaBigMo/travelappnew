//
//  Travel.swift
//  Travel App
//
//  Created by Михаил on 07/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class Travel {
    var name: String = ""
    var desc: String = ""
    var stops: [Stop] = []
    
    func getAverageRating () -> Int {
        if stops.count <= 0 {
            return 5
        }
        var summ: Int = 0
        for stop in stops{
            summ = summ + stop.rating
        }
        return summ/stops.count
    }
    
}
