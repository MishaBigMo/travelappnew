//
//  DataBaseManager.swift
//  Travel App
//
//  Created by Михаил on 29/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import RealmSwift
import Foundation

class DataBaseManager {
    static var instance = DataBaseManager()
    
    func getObjects<T: Object>(_ classType: T.Type) -> [T] {
        let realm = try! Realm()
        let result = realm.objects(T.self)
        return Array(result)
    }
    
    func deleteTravelFromDataBase(_ travel: RealmTravel) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(travel)
        }
    }
    
    func deleteStopFromDataBase(_ stop: RealmStop) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(stop)
        }
    }
    
    
    func save<T>(_ objects: T) {
        let realm = try! Realm()
        try! realm.write {
            if let object = objects as? Object {
                realm.add(object, update: true)
            }
        }
    }
    
    func save<T: Object, S>(_ objects: [T], types: S) {
        let realm = try! Realm()
        try! realm.write {
                realm.add(objects, update: true)
        }
    }
    
    func saveStopToDataBase (stops: [RealmStop]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(stops, update: true)
        }
    }
    
    func addStop(_ stop: RealmStop, to travel: RealmTravel) {
        let realm = try! Realm()
        try! realm.write {
            travel.stops.append(stop)
        }
    }
    
//    func updateStop(_ stop: RealmStop) {
//        let realm = try! Realm()
//        try! realm.write {
//            stop
//        }
//    }
    
    func updateTravel(_ travel: RealmTravel, withName name: String) {
        let realm = try! Realm()
        try! realm.write {
            travel.name = name
        }
    }
    
    
    func saveTravelToDataBase (travel: RealmTravel) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(travel, update: true)
        }
    }
    
    func getStopFromDataBase() -> [Stop] {
        let realm = try! Realm()
        let realmStopResult = realm.objects(RealmStop.self)
        var stops: [Stop] = []
        for realmStop in realmStopResult {
            let stop = Stop()
            stop.money = realmStop.money
            stop.name = realmStop.name
            stop.rank = realmStop.rank
            stop.transport = realmStop.rank
            stop.details = realmStop.details
        }
        return stops
    }
    
    func getTravelFromDataBase(withId id: String) -> RealmTravel? {
        let realm = try! Realm()
        let travel = realm.object(ofType: RealmTravel.self, forPrimaryKey: id)
        return travel
    }
    
    
    func getTravelFromDataBase() -> [RealmTravel] {
        let realm = try! Realm()
        let realmTravelResult = realm.objects(RealmTravel.self)
        
        return Array(realmTravelResult)
    }

}
