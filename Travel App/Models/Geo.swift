//
//  Geo.swift
//  Travel App
//
//  Created by Михаил on 17/05/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class Geo: Decodable {
    var lat: String = ""
    var lng: String = ""
    
    enum CodingKeys : String, CodingKey {
        case lat
        case lng
    }
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.lat = try values.decode(String.self, forKey: .lat)
        self.lng = try values.decode(String.self, forKey: .lng)
        
    }
}
